package gotrace2

import (
	"encoding/json"
	"io"
	"log"
	"os"
	"os/exec"
)

// Trace will run the provided command and capture any events emitted via the
// TRACE2 event API:
// https://git-scm.com/docs/api-trace2#_the_event_format_target
func Trace(args []string) (<-chan map[string]interface{}, *exec.Cmd, error) {
	cmd := exec.Command(args[0], args[1:]...)

	// 3 is the file descriptor we wish Git to send events to
	cmd.Env = []string{"GIT_TRACE2_EVENT=3"}

	r, w, err := os.Pipe()
	if err != nil {
		return nil, nil, err
	}

	cmd.Stdin = os.Stdin           // file descriptor 0
	cmd.Stdout = os.Stdout         // file descriptor 1
	cmd.Stderr = os.Stderr         // file descriptor 2
	cmd.ExtraFiles = []*os.File{w} // w will be file descriptor 3

	if err := cmd.Start(); err != nil {
		return nil, nil, err
	}

	dec := json.NewDecoder(r)
	eventQ := make(chan map[string]interface{})
	go func() {
		defer close(eventQ)

		for {
			m := map[string]interface{}{}
			if err := dec.Decode(&m); err != nil {
				if err == io.EOF {
					return
				}
				panic(err)
			}
			eventQ <- m
		}
		log.Print("Done")
	}()

	err = cmd.Wait()
	w.Close()
	if err != nil {
		return nil, nil, err
	}

	return eventQ, cmd, nil
}
