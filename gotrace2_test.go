package gotrace2_test

import (
	"testing"

	"gitlab.com/pokstad1/gotrace2"
)

func TestTrace(t *testing.T) {
	eventQ, _, err := gotrace2.Trace([]string{
		"git",
		"status",
	})
	if err != nil {
		t.Fatal(err)
	}

	done := make(chan struct{})
	defer func() { <-done }()

	go func() {
		defer close(done)

		for e := range eventQ {
			t.Log(e)
		}
	}()
}
